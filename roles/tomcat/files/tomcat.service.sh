[Unit]
Description=tomcat daemon
After=network.target

[Service]
Type=forking
ExecStart=/opt/tomcat/bin/catalina.sh start
ExecStop=/opt/tomcat/bin/catalina.sh stop
User=tomcat
Restart=on-abort

[Install]
WantedBy=multi-user.target

